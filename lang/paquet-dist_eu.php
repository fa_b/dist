<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-dist?lang_cible=eu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'dist_description' => 'SPIP 3rekin banatutako eskeleto jokoa.',
	'dist_slogan' => 'SPIP 3ko lehenetsitako eskeletoak'
);
