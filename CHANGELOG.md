# Changelog

## Unreleased

### Added

- Installable en tant que package Composer
- #4865 Utilisation de variables CSS

### Changed

- #4861 Balisage respectant mieux HTML5

### Fixed

- #4879 Suppression des parenthèses autour de l'url du site dans le mail envoyé à l'auteur
